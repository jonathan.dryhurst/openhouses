import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import axios from "../../shared/axios/simplyrets";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import * as actions from "../../store/actions/index";
import Listing from "../../components/Listing/Listing";
import { Card, Container, Dimmer, Loader } from "semantic-ui-react";
import { USER_ID } from "../../shared/constants";

// class based listing compoent handles state and
// will pass props down to functional components
class Listings extends Component {
  state = {};
  // fetch listings when this component
  // has mounted and is ready to deal
  // with side effects
  componentDidMount() {
    this.props.onFetchListings(); //this.props.userId
    //this.props.onFetchSavedListings();
  }

  saveListingHandler = mlsId => {
    this.props.onInitSaveListing();
    const saveListingData = {
      mlsId: mlsId,
      userId: localStorage.getItem(USER_ID)
    };
    this.props.onSaveListing(saveListingData);
  };

  render() {
    let listings = (
      <Fragment>
        <Dimmer active inverted>
          <Loader size="huge">Loading</Loader>
        </Dimmer>
      </Fragment>
    );
    if (!this.props.loading) {
      listings = this.props.listings.map(data => (
        <Listing
          image={data.listing.photos[0]}
          key={data.listing.mlsId}
          address={data.listing.address.full}
          state={data.listing.address.state}
          postalCode={data.listing.address.postalCode}
          stories={data.listing.property.stories}
          bedrooms={data.listing.property.bedrooms}
          bathsFull={data.listing.property.bathsFull}
          bathsHalf={data.listing.property.bathsHalf}
          mlsId={data.listing.mlsId}
          listDate={data.listing.listDate}
          listPrice={data.listing.listPrice}
          saveListing={this.saveListingHandler}
        />
      ));
    }
    return (
      <Fragment>
        <Container style={{ marginTop: "20px" }}>
          <Card.Group stackable doubling itemsPerRow={4}>
            {listings}
          </Card.Group>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    listings: state.listing.listings,
    loading: state.listing.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchListings: () => dispatch(actions.fetchListings()),
    onInitSaveListing: () => dispatch(actions.saveListingInit()),
    onSaveListing: saveListingData =>
      dispatch(actions.saveListing(saveListingData))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Listings, axios));
