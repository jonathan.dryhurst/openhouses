import firebase from "firebase/app";
import "firebase/firestore";
import {
  FIREBASE_PROJECT_ID,
  FIREBASE_API_KEY,
  FIREBASE_DATABASE_URL
} from "../constants";

const config = {
  apiKey: FIREBASE_API_KEY,
  databaseURL: FIREBASE_DATABASE_URL,
  projectId: FIREBASE_PROJECT_ID
};

firebase.initializeApp(config);

firebase
  .firestore()
  .enablePersistence()
  .catch(function(err) {
    if (err.code === "failed-precondition") {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
    } else if (err.code === "unimplemented") {
      // The current browser does not support all of the
      // features required to enable persistence
      // ...
    }
  });

export default firebase;
