// export const AUTH_TOKEN = "auth-token";
// export const LINKS_PER_PAGE = 5;

// key to store a random user id in localStorage
export const USER_ID = "userId";

// simplyrets test data authentication
export const SIMPLY_RETS_AUTH_USER = "simplyrets";
export const SIMPLY_RETS_AUTH_PASS = "simplyrets";

// firebase instance settings. in reality these would be
// set as environment variables most likely
export const FIREBASE_PROJECT_ID = "openhouses-66c41";
export const FIREBASE_API_KEY = "AIzaSyCoboIdJkFnJOumb4Bse6qJx9TfEaJ7g4U";
export const FIREBASE_DATABASE_URL = "https://openhouses-66c41.firebaseio.com";
