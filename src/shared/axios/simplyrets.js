import axios from "axios";
import { SIMPLY_RETS_AUTH_USER, SIMPLY_RETS_AUTH_PASS } from "../constants";

// create an axios instance that we can configure
// and use anywhere.
const instance = axios.create({
  baseURL: "https://api.simplyrets.com",
  // startdate is required to get actual data
  // from the API. failure to pass it
  // results in an empty array
  params: { startdate: "2015-01-01" },
  auth: {
    username: SIMPLY_RETS_AUTH_USER,
    password: SIMPLY_RETS_AUTH_PASS
  }
});

export default instance;
