import * as actionTypes from "./actionTypes";
import simplyrets from "../../shared/axios/simplyrets";
import firebase from "../../shared/firebase/firebase";

// save listing
export const saveListingInit = () => {
  return {
    type: actionTypes.SAVE_LISTING_INIT
  };
};

export const saveListingSuccess = (id, saveListingData) => {
  return {
    type: actionTypes.SAVE_LISTING_SUCCESS
  };
};

export const saveListingFail = error => {
  return {
    type: actionTypes.SAVE_LISTING_FAIL,
    error: error
  };
};

export const saveListingStart = () => {
  return {
    type: actionTypes.SAVE_LISTING_START
  };
};

export const saveListing = saveListingData => {
  return dispatch => {
    dispatch(saveListingStart());
    const db = firebase.firestore();
    db.collection("savedListings")
      .add(saveListingData)
      .then(ref => {
        dispatch(saveListingSuccess(ref.id, saveListingData));
      })
      .catch(error => {
        dispatch(saveListingFail(error));
      });
  };
};

// fetch listings
export const fetchListingsSuccess = listings => {
  return {
    type: actionTypes.FETCH_LISTINGS_SUCCESS,
    listings: listings
  };
};

export const fetchListingsFail = error => {
  return {
    type: actionTypes.FETCH_LISTINGS_FAIL,
    error: error
  };
};

export const fetchListingsStart = () => {
  return {
    type: actionTypes.FETCH_LISTINGS_START
  };
};

export const fetchListings = () => {
  return dispatch => {
    dispatch(fetchListingsStart());
    simplyrets
      .get("/openhouses")
      .then(response => {
        const fetchedListings = [];
        for (let key in response.data) {
          fetchedListings.push({
            ...response.data[key],
            id: key
          });
        }
        dispatch(fetchListingsSuccess(fetchedListings));
      })
      .catch(error => {
        dispatch(fetchListingsFail(error));
      });
  };
};
