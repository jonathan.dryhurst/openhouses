import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";
const initialState = {
  listings: [],
  saving: false,
  saved: false,
  loading: false
};

// save listing reducers
const saveListingInit = (state, action) => {
  return updateObject(state, { saved: false });
};

const saveListingStart = (state, action) => {
  return updateObject(state, { saving: true });
};

const saveListingSucces = (state, action) => {
  return updateObject(state, {
    saving: false,
    saved: true
  });
};

const saveListingFail = (state, action) => {
  return updateObject(state, { saving: false });
};

// fetch listings reducers
const fetchListingsStart = (state, action) => {
  return updateObject(state, { loading: true });
};

const fetchListingsSuccess = (state, action) => {
  return updateObject(state, {
    listings: action.listings,
    loading: false
  });
};

const fetchListingsFail = (state, action) => {
  return updateObject(state, { loading: false });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // save listing
    case actionTypes.SAVE_LISTING_INIT:
      return saveListingInit(state, action);
    case actionTypes.SAVE_LISTING_START:
      return saveListingStart(state, action);
    case actionTypes.SAVE_LISTING_SUCCESS:
      return saveListingSucces(state, action);
    case actionTypes.SAVE_LISTING_FAIL:
      return saveListingFail(state, action);

    // listings
    case actionTypes.FETCH_LISTINGS_START:
      return fetchListingsStart(state, action);
    case actionTypes.FETCH_LISTINGS_SUCCESS:
      return fetchListingsSuccess(state, action);
    case actionTypes.FETCH_LISTINGS_FAIL:
      return fetchListingsFail(state, action);
    default:
      return state;
  }
};

export default reducer;
