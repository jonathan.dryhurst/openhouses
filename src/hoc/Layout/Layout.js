import React, { Component, Fragment } from "react";
import { Container, Header, Icon } from "semantic-ui-react";
import classes from "./Layout.css";

class Layout extends Component {
  render() {
    return (
      <Fragment>
        <Container className={classes.Container}>
          <Header as="h1">
            <Icon name="home" />
            OpenHouse Listings
          </Header>
          <main>{this.props.children}</main>
        </Container>
      </Fragment>
    );
  }
}

export default Layout;
