import React from "react";
import { Icon } from "semantic-ui-react";
import classes from "./SaveListingButton.css";

const savelistingbutton = props => {
  const { mlsId, saveListing } = props;
  return (
    <span
      onClick={() => saveListing(mlsId)}
      className={classes.SaveListingButton}
    >
      <Icon name="heart outline" color="green" />
      Save me
    </span>
  );
};

export default savelistingbutton;
