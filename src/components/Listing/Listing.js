import React from "react";
import { Card, Grid, Icon, Image } from "semantic-ui-react";
import classes from "./Listing.css";
//import SaveListingButton from "../UI/SaveListingButton/SaveListingButton";

const listing = props => {
  // deconstruct props so we don't have to
  // type so much
  const {
    image,
    address,
    state,
    postalCode,
    stories,
    bedrooms,
    bathsFull,
    bathsHalf,
    mlsId,
    listDate,
    listPrice,
    saveListing
  } = props;

  // format the price to usd locale
  const thePrice = listPrice.toLocaleString("en-US", {
    style: "currency",
    currency: "USD"
  });

  // format the date to usa locale
  const theDate = new Date(listDate).toLocaleDateString("en-US");

  return (
    <Card raised className={classes.Listing}>
      <Image src={image} />
      <Card.Content>
        <Card.Header>
          {address}, {state}, {postalCode}
        </Card.Header>
        <Card.Meta className={classes.ListPrice}>{thePrice}</Card.Meta>
        <Card.Description>
          <Grid columns={2}>
            <Grid.Column>
              <p>Stories: {stories}</p>
              <p>Bedrooms: {bedrooms}</p>
            </Grid.Column>
            <Grid.Column>
              <p>Full Baths: {bathsFull}</p>
              <p>Half Baths: {bathsHalf}</p>
            </Grid.Column>
          </Grid>
        </Card.Description>
        <Card.Meta className={classes.ListDate}>
          <p>Listed on: {theDate}</p>
        </Card.Meta>
      </Card.Content>
      <Card.Content extra>
        {/* <SaveListingButton mlsId={mlsId} saveListing={saveListing} /> */}
        <span
          onClick={() => saveListing(mlsId)}
          className={classes.SaveListingButton}
        >
          <Icon name="heart outline" color="green" />
          Save me
        </span>
        <span style={{ float: "right" }}>mlsId: {mlsId} </span>
      </Card.Content>
    </Card>
  );
};

export default listing;
