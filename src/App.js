import React, { Component } from "react";
import Listings from "./containers/Listings/Listings";
import Layout from "./hoc/Layout/Layout";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Listings />
        </Layout>
      </div>
    );
  }
}

export default App;
