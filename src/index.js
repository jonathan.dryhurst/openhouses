import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import listingReducer from "./store/reducers/listing";
import { USER_ID } from "./shared/constants";
import { generateUserId } from "./shared/utility";

// root
const rootReducer = combineReducers({
  listing: listingReducer
});

// set up the redux devtools. show only if using
// the development environment
// https://github.com/zalmoxisus/redux-devtools-extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// create the redux store
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

// make sure this user has a unique id
if (!localStorage.getItem(USER_ID))
  localStorage.setItem(USER_ID, generateUserId()); //

// render the app
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
