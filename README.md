# Hi!

Thank you for this great opportunity with Side!

## Installing

`$ npm install`

## Running

`$ npm start`

# What did I do?

I initially started out by creating an account over on SimplyRETS
so that I could start inspecting the API endpoint and learning
more about what SimplyRETS has to offer. I was met with some
initial difficulty due to the fact that the API endpoint provided:
"https://api.simplyrets.com/openhouses" does not in and of itself
return anything other than an empty array. I immediately reached
out to the SimplyRETS support team about the issue and it turns
out they need to refresh their data, but gave me an interum
solution to send a "startdate" query paramater to get data
from the past. I launched Postman, added the Basic Auth details
the URL to the API and the startdate query parmater and bingo,
data. Not only did that work, but I had a great conversation and
possibly made a new friend! In fact we both taught each other
something in that exchange. Good stuff. Let's build the project

Next I used create-react-app to initialize a ReactJS application.
Standard stuff.

Installed the following additional dependencies that do not come
with create-react-app:

- axios
- firebase
- semantic-ui-react
- redux
- react-redux
- redux-thunk

## Axios

I am using axios handle the API calls to SimplyRets. I've
created a shared simplyrets specific axios configuartion
in a simplyrets.js file that can be imported anywhere
you need to request data from simplyrets.

`/shared/axios/simplyrets.js`

## Firebase

Firebase is obviously used to implement firebase features, but
that too has it's own shared configuration file in the shared
directory.

`/shared/firebase/firebase.js`

## Semantic UI React

Implemented Semantic UI React to do most of the view heavy lifting.
This is a great css framework for rapidly prototyping/building
projects and I really love it. You'll probably want to test the
app works well in all viewports. It looks really nice on desktop
mobile and tablets.

## Redux

Finally I am using Redux to emit actions and manage state.

## Redux thunk

For middleware

## npm run eject

I ran npm run eject so i could make changes to the webpack configuration

## CSS Modules

There are a number of ways to style components in ReactJS, but for
this demonstration I used CSS Modules. Adding some options to the
css-loader configuration is all we needed to do for this. Now
I can create css files scoped for it's intended component.
sweet.

## And finally...

I've deployed the app to firebase: https://openhouses-66c41.firebaseapp.com/
